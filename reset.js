import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';

const Reset = () => {
  const [email, setEmail] = React.useState('');

  return (
    <View style={styles.container}>
      <Image style={styles.gambar} source={require('./assets/logo.png')} />
      <Text style={styles.title}> Reset your password </Text>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Email"
          label="Email"
          value={email}
          onChangeText={email => setEmail(email)}
        />
      </View>
      <TouchableOpacity onPress={() => Alert.alert('Email telah dikirim!')}>
        <Text style={styles.request}>Request Reset</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'center',
    alignItems: 'center',
  },
  gambar: {
    marginBottom: 17,
  },
  title: {
    textAlign: 'center',
    fontSize: 16,
    color: '#5B5B5B',
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 33,
  },
  input: {
    height: 43,
    width: 273,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    width: 273,
    height: 43,
    marginBottom: 17,
    padding: 18,
    fontSize: 12,
  },
  request: {
    fontSize: 12,
    color: '#FFFFFF',
    backgroundColor: '#2E3283',
    width: 273,
    height: 43,
    textAlign: 'center',
    padding: 13,
    borderRadius: 5,
    marginTop: 24,
  },
});

export default Reset;
