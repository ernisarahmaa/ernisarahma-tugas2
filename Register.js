import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';

const Register = ({navigation}) => {
  const [name, setName] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [phone, setPhone] = React.useState('');
  const [pass, setPass] = React.useState('');

  return (
    <View style={styles.container}>
      <Image style={styles.gambar} source={require('./assets/logo.png')} />
      <Text style={styles.title}> Create an account </Text>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Name"
          label="Name"
          value={name}
          onChangeText={name => setName(name)}
        />
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Email"
          label="Email"
          value={email}
          onChangeText={email => setEmail(email)}
        />
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Phone"
          label="Phone"
          value={phone}
          onChangeText={phone => setPhone(phone)}
        />
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Password"
          label="Password"
          value={pass}
          onChangeText={pass => setPass(pass)}
          secureTextEntry={true}
        />
      </View>
      <TouchableOpacity onPress={() => Alert.alert('Berhasil Register!')}>
        <Text style={styles.signup}>Sign Up</Text>
      </TouchableOpacity>
      <View style={styles.footer}>
        <Text style={styles.textlogin}>
          <Text>Already have account?</Text>
          <Text
            onPress={() => navigation.navigate('Login')}
            style={{fontWeight: 'bold'}}>
            {' '}
            Log In
          </Text>
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'center',
    alignItems: 'center',
  },
  gambar: {
    marginBottom: 17,
  },
  title: {
    textAlign: 'center',
    fontSize: 16,
    color: '#5B5B5B',
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 33,
  },
  input: {
    height: 43,
    width: 273,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    width: 273,
    height: 43,
    marginBottom: 17,
    padding: 18,
    fontSize: 12,
  },
  signup: {
    fontSize: 12,
    color: '#FFFFFF',
    backgroundColor: '#2E3283',
    width: 273,
    height: 43,
    textAlign: 'center',
    padding: 13,
    borderRadius: 5,
    marginTop: 24,
  },
  footer: {
    backgroundColor: 'white',
    height: 60,
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
  textlogin: {
    textAlign: 'center',
    paddingVertical: 23,
    fontSize: 11,
  },
});

export default Register;
