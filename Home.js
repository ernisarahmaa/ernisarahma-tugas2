import React from 'react';
import {
  Image,
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  Text,
  Alert,
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MyTabBar from './BottomTabs';

function HomeScreen() {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.content}>
          <View style={styles.imageContainer}>
            <Image
              style={styles.banner}
              source={require('./assets/banner.png')}
            />
          </View>
          <View style={styles.menu}>
            <View style={styles.menuItem}>
              <Image source={require('./assets/ic_ferry_intl.png')} />
            </View>
            <View style={styles.menuItem}>
              <Image source={require('./assets/ic_ferry_domestic.png')} />
            </View>
            <View style={styles.menuItem}>
              <Image source={require('./assets/ic_attraction.png')} />
            </View>
            <View style={styles.menuItem}>
              <Image source={require('./assets/ic_pioneership.png')} />
            </View>
          </View>
          <TouchableOpacity
            style={styles.button}
            onPress={() => Alert.alert('Comming Soon')}>
            <Text
              style={{color: 'white', fontWeight: 'bold', textAlign: 'center'}}>
              More ...
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

function MyBookingScreen() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>My Booking!</Text>
    </View>
  );
}

function HelpScreen() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Help</Text>
    </View>
  );
}

function ProfileScreen() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Profile</Text>
    </View>
  );
}

const Tab = createBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator
      tabBar={props => <MyTabBar {...props} />}
      screenOptions={{headerShown: false}}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="My Booking" component={MyBookingScreen} />
      <Tab.Screen name="Help" component={HelpScreen} />
      <Tab.Screen name="Profile" component={ProfileScreen} />
    </Tab.Navigator>
  );
}

const Home = () => {
  return <MyTabs />;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
  },
  content: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    alignItems: 'center',
  },
  imageContainer: {
    flexDirection: 'row',
  },
  banner: {
    flex: 1,
    resizeMode: 'cover',
    aspectRatio: 2.5,
  },
  menu: {
    flexDirection: 'row',
    margin: 22,
    justifyContent: 'center',
  },
  menuItem: {
    flex: 1,
    marginHorizontal: 10,

    alignItems: 'center',
  },
  button: {
    backgroundColor: '#2E3283',
    justifyContent: 'center',
    height: 40,
    width: 140,
    marginHorizontal: 126,
    borderRadius: 5,
    marginTop: 400,
    marginBottom: 100,
    fontWeight: 'bold',
  },
});

export default Home;
