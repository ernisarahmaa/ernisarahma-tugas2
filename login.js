import React from 'react';
import TextBox from 'react-native-password-eye';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';

const Login = ({navigation}) => {
  const [name, setName] = React.useState('');
  const [pass, setPass] = React.useState('');

  return (
    <View style={styles.container}>
      <Image style={styles.gambar} source={require('./assets/logo.png')} />
      <Text style={styles.title}> Please sign in to continue </Text>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Username"
          label="Username"
          value={name}
          onChangeText={name => setName(name)}
        />
      </View>
      <View style={styles.inputContainer}>
        <TextBox
          containerStyle={styles.input}
          placeholder="***********"
          label="Password"
          value={pass}
          onChangeText={pass => setPass(pass)}
          secureTextEntry={true}
          eyeColor="#2E2E2E"
        />
      </View>
      <TouchableOpacity onPress={() => navigation.navigate('Home')}>
        <Text style={styles.signin}>Sign in</Text>
      </TouchableOpacity>
      <Text
        onPress={() => navigation.navigate('Reset')}
        style={styles.forgotlogin}>
        Forgot Password
      </Text>
      <View style={styles.lineContainer}>
        <View style={styles.line} />
        <View>
          <Text style={styles.forgotlogin}>
            {'         '}
            Login with
            {'         '}
          </Text>
        </View>
        <View style={styles.line} />
      </View>
      <View style={styles.rowContainer}>
        <TouchableOpacity onPress={() => Alert.alert('Button Pressed!')}>
          <Image
            style={styles.optionVersion}
            source={require('./assets/fb.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => Alert.alert('Button Pressed!')}>
          <Image
            style={styles.optionVersion}
            source={require('./assets/google.png')}
          />
        </TouchableOpacity>
      </View>
      <View style={styles.rowContainer}>
        <Text style={styles.optionVersion}>App Version</Text>
        <Text style={styles.optionVersion}>2.8.3</Text>
      </View>
      <View style={styles.footer}>
        <Text style={styles.textsignup}>
          <Text>Don't have account?</Text>
          <Text
            onPress={() => navigation.navigate('Register')}
            style={{fontWeight: 'bold'}}>
            {' '}
            Sign up
          </Text>
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'center',
    alignItems: 'center',
  },
  gambar: {
    marginBottom: 17,
  },
  title: {
    textAlign: 'center',
    fontSize: 16,
    color: '#5B5B5B',
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 33,
  },
  input: {
    height: 43,
    width: 273,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    width: 273,
    height: 43,
    marginBottom: 17,
    paddingHorizontal: 18,
    fontSize: 12,
  },
  signin: {
    fontSize: 12,
    color: '#FFFFFF',
    backgroundColor: '#2E3283',
    width: 273,
    height: 43,
    textAlign: 'center',
    padding: 13,
    borderRadius: 5,
    marginTop: 24,
  },
  forgotlogin: {
    color: '#2E3283',
    marginTop: 17,
  },
  lineContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 270,
    marginTop: 33,
  },
  line: {
    flex: 1,
    height: 1,
    backgroundColor: '#2E3283',
    marginTop: 17,
  },
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  optionVersion: {
    margin: 29,
  },
  footer: {
    backgroundColor: 'white',
    height: 60,
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
  textsignup: {
    textAlign: 'center',
    paddingVertical: 23,
    fontSize: 11,
  },
});

export default Login;
