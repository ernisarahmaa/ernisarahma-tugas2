import {View, Text, TouchableOpacity, StyleSheet, Image} from 'react-native';
import React from 'react';

function MyTabBar({state, descriptors, navigation}) {
  return (
    <View style={styles.bottom}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        const getIcon = label => {
          switch (label) {
            case 'Home':
              return require('./assets/Home.png');
            case 'My Booking':
              return require('./assets/MyBooking.png');
            case 'Help':
              return require('./assets/Help.png');
            case 'Profile':
              return require('./assets/Profile.png');
          }
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{flex: 1, alignItems: 'center'}}>
            <Image source={getIcon(label)} />
            <Text
              style={{
                color: isFocused ? '#673ab7' : '#222',
              }}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const styles = StyleSheet.create({
  bottom: {
    flexDirection: 'row',
    backgroundColor: 'white',
    height: 60,
  },
});

export default MyTabBar;
